@ECHO OFF
setlocal enabledelayedexpansion
setlocal
ECHO Batch script for extract classes with selected height from LAS
ECHO Author: Marek Szczepkowski
ECHO Date: 31.03.2020
ECHO Company: Visimind Ltd
ECHO Version: 1.0
ECHO For SAGA 6.0, LASTool, QGIS 2.14
ECHO.

REM USER DEFINE:
SET SAGA_ROOT="d:\MSZ\__SCRIPTS, COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\saga-6.0.0_x64\"
SET QGIS_ROOT="C:\Program Files\QGIS 3.8"
SET LASTOOLS_ROOT="d:\MSZ\__SCRIPTS, COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\LAStools\"
SET CLASS_DOWN=16
SET CLASS_UP=19
SET H_MIN=0.5
SET H_MAX=3
SET CELL_SIZE=0.25

REM Setup saga_cmd.exe
SET PATH=%PATH%;%SAGA_ROOT%
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
REM Setup txt2las.exe
SET xyz2las=%LASTOOLS_ROOT%\bin\txt2las.exe

REM Path to working dir
SET WORK=%cd%
REM Suffix output files
SET OUT_SUFFIX=_Houses

REM CREATE LEGEND
(
echo 0 255 0 0 
echo -9999 0 0 0) > color.txt
 

REM COUNTER FILES
dir /b *.las 2> nul | find "" /v /c > tmp && set /p countLAS=<tmp && del tmp
set /A Counter=1
FOR /F %%i IN ('dir /b "%WORK%\*.las"') DO (
	ECHO. Processing  %%i....    !Counter! / %countLAS% FILES	
	ECHO.   1/9 EXTRACT HOUSES
	saga_cmd --flags=r io_shapes_las "Import LAS Files" -FILES:"%WORK%\%%i" -POINTS:"%WORK%\%%i" -RGB_RANGE:0 -c=1
    saga_cmd --flags=r pointcloud_tools "Point Cloud Reclassifier / Subset Extractor" -INPUT:"%WORK%\%%~ni.sg-pts" -ATTRIB:"classification" -MODE:1 -METHOD:1 -OLD:1 -MIN=%CLASS_DOWN% -MAX=%CLASS_UP% -ROPERATOR=0 -RESULT:"%WORK%\%%~ni_reclassify.sg-pts"

	FOR %%A IN ("M-33-10-A-a-3-3-4_reclassify.sg-pts") DO if %%~zA EQU 63 (
		DEL /Q "%WORK%\*.sg-pts"
		DEL /Q "%WORK%\*.sg-info"
	) else (
		
		ECHO.   2/9 DTM   
		saga_cmd --flags=r pointcloud_tools "Point Cloud Reclassifier / Subset Extractor" -INPUT:"%WORK%\%%~ni.sg-pts" -ATTRIB:"classification" -MODE:1 -METHOD:0 -OLD:1 -SOPERATOR:0 -RESULT:"%WORK%\%%~ni_ground.sg-pts"
		saga_cmd --flags=r pointcloud_tools "Point Cloud to Grid" -POINTS="%WORK%\%%~ni_ground.sg-pts" -GRID="%WORK%\%%~ni.sgrd" -OUTPUT:0 -AGGREGATION:3 -CELLSIZE:0.500000
		saga_cmd --flags=r grid_tools "Close Gaps" -INPUT:"%WORK%\%%~ni.sgrd" -RESULT:"%WORK%\%%~ni_closed_gaps.sgrd" -THRESHOLD:0.100000
	
		ECHO.   3/9 DISTANCE TO GRID
		saga_cmd --flags=r shapes_grid 1 -SHAPES="%WORK%\%%~ni_reclassify.sg-pts" -GRIDS="%WORK%\%%~ni_closed_gaps.sgrd" -RESULT="%WORK%\%%~ni_points.sg-pts" -RESAMPLING=0
		
		ECHO.   4/9 Calculate distance to ground
		saga_cmd --flags=r table_calculus 2 -TABLE="%WORK%\%%~ni_points.shp" -FORMULA=f3-f5 -SELECTION=1 -FIELD:"Changed Gr"  -RESULT="%WORK%\%%~ni_calc.sg-pts"
	
		ECHO.   5/9 Points to pointcloud
		saga_cmd --flags=r pointcloud_tools 3 -SHAPES="%WORK%\%%~ni_calc.shp" -ZFIELD=-1 -OUTPUT=1 -POINTS="%WORK%\%%~ni_pc.sg-pts"
		
		ECHO.   6/9 RECLASS BY HEIGHT
		saga_cmd --flags=r pointcloud_tools "Point Cloud Reclassifier / Subset Extractor" -INPUT:"%WORK%\%%~ni_pc.sg-pts" -ATTRIB:"Calculation" -MODE:1 -METHOD:1 -OLD:1 -MIN=%H_MIN% -MAX=%H_MAX% -ROPERATOR=0 -RESULT:"%WORK%\%%~ni_hight.sg-pts"
		
		ECHO.   7/9 POINT CLOUD TO GRID
		saga_cmd --flags=r pointcloud_tools "Point Cloud to Grid" -POINTS="%WORK%\%%~ni_hight.sg-pts" -GRID="%WORK%\%%~ni_final.sgrd" -OUTPUT:0 -AGGREGATION:3 -CELLSIZE:%CELL_SIZE%
				
		ECHO.   8/9 Export to tiff
		saga_cmd --flags=r  io_gdal 2 -GRIDS="%WORK%\%%~ni_final.sgrd" -FILE="%WORK%\%%~ni_final.tif" -OPTIONS=TFW=YES -OPTIONS=TFW=YES 
		
		ECHO.   9/9 Tiff Change color
		gdaldem color-relief -of GTiff -alpha -co "TFW=YES" "%WORK%\%%~ni_final.tif" "color.txt" "%WORK%\%%~ni_colored.tif" 

		DEL /Q "%WORK%\%%~ni_closed_gaps.*"
		DEL /Q "%WORK%\*.mgrd"
		DEL /Q "%WORK%\*.sdat" 
		DEL /Q "%WORK%\*.sgrd"
		DEL /Q "%WORK%\*final.*"
		DEL /Q "%WORK%\*points.*"
		DEL /Q "%WORK%\*calc.*"
		DEL /Q "%WORK%\*.prj"
		DEL /Q "%WORK%\*.sg-pts"
		DEL /Q "%WORK%\*.sg-info"
		)
		set /A Counter+=1
)
DEL /Q "%WORK%\*.txt"
ECHO SCRIPT ENDS WITHOUT ERRORS
PAUSE